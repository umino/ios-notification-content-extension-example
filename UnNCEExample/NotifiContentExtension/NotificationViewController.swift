//
//  NotificationViewController.swift
//  NotifiContentExtension
//
//  Created by phuongnl on 5/14/18.
//  Copyright © 2018 phuongnl. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI
import SDWebImage

class NotificationViewController: UIViewController, UNNotificationContentExtension {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet var taskName: UILabel!
    @IBOutlet weak var taskDescription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
    }
    
    func didReceive(_ notification: UNNotification) {
        taskName.text = notification.request.content.title
        taskDescription.text = notification.request.content.body
        if let imageURL = notification.request.content.userInfo["imageURL"] as? String {
            image.sd_setImage(with: URL(string: imageURL))
        }

    }

}
