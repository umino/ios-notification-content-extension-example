//
//  UNNotificationAttachment.swift
//  UnNCEExample
//
//  Created by phuongnl on 5/14/18.
//  Copyright © 2018 phuongnl. All rights reserved.
//

import Foundation
import UserNotifications

@available(iOS 10.0, *)
extension UNNotificationAttachment {
    // save image as temporary on disk
    static func create (imageFileIdentifier: String, data: NSData, options: [NSObject: AnyObject]?) -> UNNotificationAttachment? {
        let fileManager = FileManager.default
        let tempSubFolderName = ProcessInfo.processInfo.globallyUniqueString
        let tempSubFolderURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(tempSubFolderName, isDirectory: true)
        
        do {
            try fileManager.createDirectory(at: tempSubFolderURL, withIntermediateDirectories: true, attributes: nil)
            let imageIdentifier = imageFileIdentifier + ".png"
            let fileURL = tempSubFolderURL.appendingPathComponent(imageIdentifier)
            try data.write(to: fileURL, options: [])
            let imageAttachment = try UNNotificationAttachment(identifier: imageFileIdentifier, url: fileURL, options: options)
            
            return imageAttachment
        } catch let error {
            print("Error when save image as temporary: \(error)")
        }
        return nil
    }
}
