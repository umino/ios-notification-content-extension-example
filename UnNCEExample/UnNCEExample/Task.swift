//
//  Task.swift
//  UnNCEExample
//
//  Created by phuongnl on 5/10/18.
//  Copyright © 2018 phuongnl. All rights reserved.
//

import Foundation

class Task {
    var id: Int!
    var name: String!
    var description: String!
    var dateTime: Date!
    var imageUrl: String!
    var isFinished: Bool!
    private static var idCounter: Int = 0
    
    init (name: String, dateTime: Date, description: String) {
        self.id = Task.idCounter
        self.name = name
        self.description = description
        self.dateTime = dateTime
        self.isFinished = false
        
        self.imageUrl = "https://gitlab.com/umino/ios-notification-content-extension-example/raw/0b581bc6f8332e800687edad2252f65c22c1f256/UnNCEExample/UnNCEExample/alarm.png"
        Task.idCounter += 1
    }
}
