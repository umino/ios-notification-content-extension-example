//
//  ViewController.swift
//  UnNCEExample
//
//  Created by phuongnl on 5/10/18.
//  Copyright © 2018 phuongnl. All rights reserved.
//

import UIKit
import UserNotifications
import MobileCoreServices

class ViewController: UIViewController {
    @IBOutlet weak var tfTaskName: UITextField!
    @IBOutlet weak var tfTaskDescription: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btnAddTask: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var taskList = [Task]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setView()
        btnAddTask.addTarget(self, action: #selector(self.btnAddTaskTapped(_:)), for: .touchUpInside)
        tableView.reloadData()
    }
    
    func setView() {
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .none
    }
    
    @objc func btnAddTaskTapped (_ sender: AnyObject) {
        guard let taskName = tfTaskName.text?.trimmingCharacters(in: .whitespaces) else {
            return
        }
        if taskName == "" {
            return
        }
        
        let newTask = Task(name: tfTaskName.text!, dateTime: datePicker.date, description: tfTaskDescription.text ?? "")
        taskList.append(newTask)
        tableView.reloadData()
        
        tfTaskName.text = ""
        tfTaskDescription.text = ""
        datePicker.setDate(Date(), animated: false)
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as? TaskCell else {
            fatalError("The dequeued cell is not an instance of Cell.")
        }
        let task = taskList[indexPath.row]
        cell.set(for: task)
        return cell
    }
}
































