//
//  TaskCell.swift
//  UnNCEExample
//
//  Created by phuongnl on 5/10/18.
//  Copyright © 2018 phuongnl. All rights reserved.
//

import UIKit
import UserNotifications

class TaskCell: UITableViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var lbTaskName: UILabel!
    @IBOutlet weak var lbTaskDescription: UILabel!
    @IBOutlet weak var lbTaskTime: UILabel!
    @IBOutlet weak var btnFinishTask: UIButton!
    var task: Task!
    
    func set(for task: Task) {
        self.task = task
        setView()
        
        lbTaskName.text = task.name
        lbTaskDescription.text = task.description
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm,  MMM dd yyyy"
        lbTaskTime.text = dateFormatter.string(from: task.dateTime)
        btnFinishTask.addTarget(self, action: #selector(self.btnFinishTaskTapped(_:)), for: .touchUpInside)
        
        if #available(iOS 10.0, *) {
            retrieveNotification(for: task) { (request) in
                if request != nil || task.isFinished {
                    // Notification for this task has already exist or this task is done, no need for creating new task
                    return
                }
                self.createReminderNotification(for: task)
            }
        } else {
            // in iOS lower version, Apple not allows for getting scheduledLocalNotifications so cannot avoid repettion
        }
    }
    
    func setView() {
        cellView.layer.borderColor = cellView.backgroundColor?.cgColor
        cellView.layer.borderWidth = 1
        cellView.layer.cornerRadius = 6
        cellView.layer.masksToBounds = false
        
        if task.dateTime.timeIntervalSinceNow <= 0 && !task.isFinished {
            task.isFinished = true
            setTaskFinished()
        }
    }
    
    
    @objc func btnFinishTaskTapped(_ sender: AnyObject) {
        task.isFinished = true
        setTaskFinished()
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [String(task.id)])
        } else {
            var pendingNotifications = UIApplication.shared.scheduledLocalNotifications
            if let notiIndex = pendingNotifications?.index(where: { $0.userInfo?["id"] as? Int == task.id }) {
                pendingNotifications?.remove(at: notiIndex)
            }
        }
    }
    
    func setTaskFinished() {
        btnFinishTask.isEnabled = false
        btnFinishTask.isHidden = true
        cellView.backgroundColor = UIColor(red: 219/255, green: 219/255, blue: 219/255, alpha: 1)
        cellView.layer.borderColor = cellView.backgroundColor?.cgColor
        lbTaskName.textColor = UIColor.darkText
    }
}

// MARK: for notification
extension TaskCell {
    func createReminderNotification(for task: Task) {
        let time = task.dateTime.timeIntervalSinceNow
        
        if #available(iOS 10.0, *) {
            let content = UNMutableNotificationContent()
            content.title = task.name
            content.body = task.description
            content.sound = UNNotificationSound.default()
            content.categoryIdentifier = Identifiers.reminderCategory.rawValue
            content.userInfo = ["imageURL" : task.imageUrl]
            
            
            if let data = NSData(contentsOf: URL(string: task.imageUrl)!),
                let attachment = UNNotificationAttachment.create(imageFileIdentifier: "image", data: data, options: nil) {
                content.attachments = [attachment]
            }
            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: time, repeats: false)
            //let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
            let notifiId = String(task.id)
            
            let request = UNNotificationRequest(identifier: notifiId, content: content, trigger: trigger)
            UNUserNotificationCenter.current().add(request) { (error) in
                if let error = error {
                    print("Problem adding notification: \(error.localizedDescription)")
                    return
                }
            }
        } else {
            let notification = UILocalNotification()
            if #available(iOS 8.2, *) {
                notification.alertTitle = task.name
                notification.alertBody = task.description
            } else {
                notification.alertBody = task.name
                
            }
            notification.fireDate = task.dateTime
            //notification.fireDate = Date(timeIntervalSinceNow: 5)
            notification.applicationIconBadgeNumber = 0
            notification.soundName = UILocalNotificationDefaultSoundName
            notification.userInfo = ["id": task.id]
            
            UIApplication.shared.scheduleLocalNotification(notification)
        }
        
    }
    
    @available(iOS 10.0, *)
    func retrieveNotification(for task: Task, completion: @escaping (UNNotificationRequest?) -> ()) {
        UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
            DispatchQueue.main.async {
                let request = requests.filter { $0.identifier == String(task.id  ) }.first
                completion(request)
            }
        }
    }
    
//    func retrieveNotificationLowerVersion(for task: Task, completion: @escaping (UILocalNotification?) -> ()) {
//        DispatchQueue.main.async {
//            let pendingNotifications = UIApplication.shared.scheduledLocalNotifications
//            let notification = pendingNotifications?.filter { $0.userInfo?["id"] as? Int == task.id }.first
//            completion(notification)
//        }
//    }
}
