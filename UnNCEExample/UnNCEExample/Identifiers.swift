//
//  Identifiers.swift
//  UnNCEExample
//
//  Created by phuongnl on 5/14/18.
//  Copyright © 2018 phuongnl. All rights reserved.
//

import Foundation

enum Identifiers: String {
    case reminderCategory = "reminder"
    case finishAction = "finish"
}
